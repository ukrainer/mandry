<div class="main-content hidden"></div>
<div class="mainPage">
	<div class="cloudsBox">
		<div class="mainCloud">
			<p><strong>Обирайте</strong>,<br> що вам ближче до душі <br><strong>і гайда з нами:</strong></p>
		</div>
		
		<div class="clouds2 init"></div> 
		<div class="clouds2 animated"></div> 
		<div class="clouds2"></div> 

		<div class="clouds3 init"></div>
		<div class="clouds3 animated"></div>
		<div class="clouds3"></div>
	</div>	

	<div class="mainPage-left">
		<div class="bg-gradient"><img src="images/bg-main-travel.png" alt=""/><span class="vfix"></span></div>
		<div class="mainPage-phone">
			<p class="phone-title">Завжди на зв'язку:</p>
			<p><a href="tel:+38012345678" class="phone-number"><span class="phone-code">+38 (012)</span>345-67-89</a></p>
		</div>
		<div class="mainPage-button hiddenRight" data-section="travel">
			<div class="mainPage-button-title">ПОДОРОЖІ</div>
			<div class="mainPage-button-subtitle">для тих хто відпочиває з природою</div>
		</div>
	</div>
	<div class="mainPage-right">
		<div class="bg-gradient"><img src="images/bg-main-excursions.png" alt=""/><span class="vfix"></span></div>
		<div class="mainPage-lang">
			<div class="lang box">
				<div class="lang-heading" title="Language select/выбрать язык">Обрати мову:</div>
				<a href="#" class="active"><img src="images/icon-ua.png" alt=""/></a>
				<a href="#"><img src="images/icon-en.png" alt=""/></a>
				<a href="#"><img src="images/icon-ru.png" alt=""/></a>
			</div>
		</div>
		<div class="mainPage-button hiddenLeft" data-section="excursions">
			<div class="mainPage-button-title">ЕКСКУРСІЇ</div>
			<div class="mainPage-button-subtitle">для тих хто шукає відпочинок на вік-єнд</div>
		</div>
	</div>
</div>
