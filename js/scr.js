//var scroller=jQuery.browser.webkit ? "body": "html";

/* modernize */
function modernize() {
	// placeholder 
	if(!Modernizr.input.placeholder){
		$('[placeholder]').each(function() {
			$(this).watermark($(this).attr('placeholder'));
		});
	}
}

/* iosSliderInit */
function iosSliderInit() {
	var sdelay = parseInt($('#sdelay').val());
	sdelay = isNaN(sdelay) ? 5000 : sdelay;

	// set slide title from first slide alt
	setSlideTitle(0);
	
	$('.mainSlider .iosslider').iosSlider({
		snapToChildren: true,
		scrollbar: false,
		scrollbarHide: true,
		desktopClickDrag: true,
		navPrevSelector: $('.mainSlider .slide-prev'),
		navNextSelector: $('.mainSlider .slide-next'),
		infiniteSlider:  true,
		onSlideChange: slideChange,
		autoSlide: sdelay,
		autoSlideTimer: sdelay
	});
	
	
	function slideChange(args) {
		setSlideTitle(args.currentSlideNumber-1);
	}

	function setSlideTitle(slideNum) {
		$('.mainSlider .slide-title')
			.removeClass('active')
			.eq(slideNum).addClass('active');
	}
}


/* fancybox */
function fancyboxInit() {
	$('.fancybox').fancybox({
		padding:[20,25,20,25],
		scrolling: 'no',
		fitToView: false,
		autoSize : true,
		helpers : {
     		overlay : {
     			 locked : false  
     		}
		}
	});
}


/* calendar init */
function calendarInit() {
	var eventDaysArray,
		localDaysNamesMin,
		localMonthNamesMin,
		flag = true; 

	switch ($('#lang').val()) {
		case 'ru' : 
			localDaysNamesMin = ["ВС", "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ" ];
			localMonthNamesMin = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
		break;
		// default ua
		default : 
			localDaysNamesMin = ["НД", "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ" ];
			localMonthNamesMin = ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"];
		break;
	}

	$('#calendar').datepicker({	
		prevText : "&lt;",
		nextText : "&gt;",
		dateFormat : 'dd.mm.yy',
		firstDay: 1,
		dayNamesMin: localDaysNamesMin,
		monthNames : localMonthNamesMin,
		onChangeMonthYear: function() {
								flag = true;
							},
		beforeShowDay: function (date) {
							var month = date.getMonth()+1,
								year = date.getFullYear(),
							  	theDay = date.getDate();
							
							if (flag) {
								getEventDays(month, year);
								flag = false;
							}
							
							return [true, $.inArray(theDay, eventDaysArray) >= 0 ? "hasEvent" : 'ui-datepicker-unselectable ui-state-disabled'];
						},
		onSelect: function(date) {
			// url after click on hasevent day
			 document.location.href="/?eventdate="+date;
		}

	});


	// get eventdays date
	function getEventDays (month, year) {

		$.ajax({
			url : '/forajax.php',
			method : 'post',
			async : false,
			data : {selectedMonth:month, selectedYear : year,  act: "get_eventdays"},
			success : function(response) {
						eventDaysArray = $.parseJSON(response);
					}
		});
	}
	 
}

// start next layer animationa after half first animation
function startHalfDuration(block) {
	var $block = $(block) ,
	halfDuration = parseInt($block.css('animation-duration'))/2*1000;

	setTimeout(function() {
		$block.not('.animated').not('.init').addClass('animated')
	}, halfDuration)
}

/* mainPage animation */
function mainPageAnim() {
	var $leftBtn = $('.mainPage-left .mainPage-button'),
		$rightBtn = $('.mainPage-right .mainPage-button');
		
	// show buttons
	$leftBtn.removeClass('hiddenRight');
	setTimeout(function() {
		$rightBtn.removeClass('hiddenLeft');
	}, 600);


	
	startHalfDuration('.clouds2');
	startHalfDuration('.clouds3');
	


	// button click 
	$('.mainPage-button').click(function() {
		var section = $(this).data('section');

		
		
		$.ajax({
			url : "/"+section+".php",
			method: 'post',
			success: function(response) {
				$('.main-content')
					.html(response)
					.removeClass('hidden');
				$('body').addClass(section) ;
				$('.cloudsBox').fadeOut(800);

				setTimeout(function() {
					$('.mainPage-left').addClass('hiddenLeft');
					$('.mainPage-right').addClass('hiddenRight');
					initAll();
				}, 900);
							
				setTimeout(function() {
					$('.mainPage').remove();
				}, 3000);
			}

		})	
		
	});
}


/* all init function */
function initAll() {
	iosSliderInit();
	calendarInit();
	mainPageAnim();
	fancyboxInit();
}

/* DOCUMENT READY */
$(document).ready(function() {
	modernize();

	if ($('.mainPage').length) {
		mainPageAnim();
	} else {
		initAll();
	}
});



