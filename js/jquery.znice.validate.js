/*
 * zNice validation plugin
 * version: 0.1 (07.12.2013)
 * by Vitaliy Grozinskiy (zendo@ukr.net) 
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * jQuery validation (http://jqueryvalidation.org/) is required for this plugin.
 * 
 ******************************************** */
 (function($){
	/*Adding custom rules*/
	jQuery.validator.addMethod("phone", function(value, element) {
		return this.optional(element) || /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(value);
	}, "неправильний номер телефону");
	
	$.fn.zValidate = function(settings){
		var self=this;
		$(self).each(function(){
			var $form=$(this).is('form') ? $(this) : $(this).find('form');
			if ($form.length!=0){
				$form.validate({
					errorClass : 'zNice-error-text',
					invalidHandler: function(event, validator) {
						
					},
					errorPlacement: function(error, element) {
						error.appendTo( element.closest(".zNice-wrap,.zNiceInputWrapper,.zNiceSelectWrapper,.jCheckWrapper,.jRadioWrapper"));
					},
					highlight: function(element, errorClass, validClass) {
						$(element).closest('.zNice-wrap,.zNiceInputWrapper,.zNiceSelectWrapper,.jCheckWrapper,.jRadioWrapper').addClass('zNice-error').removeClass('zNice-valid');
					},
					unhighlight: function(element, errorClass, validClass) {
						$(element).closest('.zNice-wrap,.zNiceInputWrapper,.zNiceSelectWrapper,.jCheckWrapper,.jRadioWrapper').removeClass('zNice-error').addClass('zNice-valid');
					}
				});

				var requiredMess,emailMess, phoneMess;
				switch ($('#lang').val()) {
					case 'ru' :
						requiredMess = "вы пропустили";
						emailMess = "неверный email"
						phoneMess = "неправильный номер телефона";
					break;
					case 'en' :
						requiredMess= "empty field";
						emailMess = "incorrect email";
						phoneMess = "incorrect phone number";
					break;
					default:
						requiredMess = "ви пропустили";
						emailMess = "неправильний email";
						phoneMess = "неправильний номер телефону";
					break;
				}

				$('[required]',$form).each(function(){
					$(this).rules( "add", {
						required: true,
						messages: {
							required: requiredMess
						}
					});
				})
				$('[type="email"]',$form).each(function(){
					$(this).rules( "add", {
						messages: {
							email: emailMess
						}
					});
				})
				$('[phone]',$form).each(function(){
					$(this).rules( "add", {
						 phone: true,
						 messages: {
							phone:phoneMess
						}
					});
				})
			}
		})
	}
})(jQuery)