<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<title>Пройдисвіт</title>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/reset.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
	<script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>

	<script src="js/selectivizr-min.js" type="text/javascript"></script>
	<script src="js/modernizr.js" type="text/javascript"></script>
	<script src="js/jquery.watermark.min.js" type="text/javascript"></script>

	<!-- jquery ui datepicker -->
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" media="all" />
	<script src="js/jquery-ui.min.js" type="text/javascript"></script>	

	<link rel="stylesheet" type="text/css" href="css/jquery.znice.css" media="all" />
	<script src="js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="js/jquery.znice.validate.js" type="text/javascript"></script>
	<script src="js/jquery.znice.js" type="text/javascript"></script>

	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" media="all" />
	<script src="fancybox/jquery.fancybox.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.iosslider.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/iosslider.css" media="all" />

	<script src="js/scr.js" type="text/javascript"></script>

<!-- <script src="develop/autocssrenew.js" type="text/javascript"></script> -->

	<!--[if lt IE 9]>
		<script>
		document.createElement('header');
		document.createElement('nav');
		document.createElement('section');
		document.createElement('article');
		document.createElement('aside');
		document.createElement('footer');
		document.createElement('time');
		</script>	
		<script src="js/pie.js" type="text/javascript"></script>
	<![endif]-->

	<!--[if lt IE 8]><script src="js/oldie/warning.js"></script><script>window.onload=function(){e("js/oldie/")}</script><![endif]-->
</head>

<? 	$bodyClass="";
	if ($act == "travel") {
		$bodyClass="class='travel'";
	} 
	if ($act == "excursions") {
		$bodyClass="class='excursions'";
	}?>
<body <?echo $bodyClass;?>>
<header class="header">
	<div class="header-top">
		<div class="mbox">
			<nav class="header-menu">
				<ul>
					<li class="active"><a href="#">Головна</a></li>
					<li><a href="#">Про нас</a></li>
					<li><a href="#">Екскурсії та походи</a></li>
					<li><a href="#">Підготовка</a></li>
					<li><a href="#">Фотогалерея</a></li>
					<li><a href="#">Відгуки</a></li>
					<li><a href="#">Контакти</a></li>
				</ul>
			</nav>

			<div class="header-button">
				<a href="#order" class="button fancybox">ЗАЯВКА НА ПОДОРОЖ</a>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="mbox">
			<a href="/" class="logo"><img src="images/logo.png" alt=""/></a>
			
			<div class="header-phone">
				<p class="phone-title">Завжди на зв'язку:</p>
				<p><a href="tel:+38012345678" class="phone-number"><span class="phone-code">+38 (012)</span>345-67-89</a></p>
			</div>

			<div class="lang box rope">
				<div class="lang-heading" title="Language select/выбрать язык">Обрати мову:</div>
				<a href="#" class="active"><img src="images/icon-ua.png" alt=""></a>
				<a href="#"><img src="images/icon-en.png" alt=""></a>
				<a href="#"><img src="images/icon-ru.png" alt=""></a>
				
				<!-- ua en ru -->
				<input type="hidden" id="lang" value="ua"/>
			</div>

			<div class="header-social box">
				<div class="social">
					<a href="#" class="social-item icon-vk"></a>
					<a href="#" class="social-item icon-fb"></a>
					<a href="#" class="social-item icon-gg"></a>
					<a href="#" class="social-item icon-inst"></a>
				</div>
			</div>
		</div>
	</div>
</header>
