	<div class="footer_placeholder"></div>
	<footer class="footer">
		<div class="mbox">
			<div class="footer-left">
				<div class="social">
					<div class="social-heading subheading">СОЦ. МЕРЕЖІ:</div>
					<div class="social-items rope">
						<a href="#" class="social-item icon-vk"></a>
						<a href="#" class="social-item icon-fb"></a>
						<a href="#" class="social-item icon-gg"></a>
						<a href="#" class="social-item icon-inst"></a>
					</div>
				</div>
			</div>

			<div class="footer-right">
				<nav class="footer-menu">
					<ul>
						<li><a href="#">Головна</a></li>
						<li><a href="#">Про нас</a></li>
						<li><a href="#">Екскурсії та походи</a></li>
						<li><a href="#">Підготовка</a></li>
						<li><a href="#">Фотогалерея</a></li>
						<li><a href="#">Відгуки</a></li>
						<li><a href="#">Статті</a></li>
						<li><a href="#">Контакти</a></li>
					</ul>
				</nav>

				<div class="subscribe">
					<form action="#" class="zNice">
						<div class="form-col">
							<p>Отримуйте найсвіжішу інформацію:</p>
						</div>
						<div class="form-col">
							<input type="email" name="subscr_email" required placeholder="E-mail" data-image="images/icon-mail.png"/>
						</div>
						<div class="form-col">
							<input type="submit" class="button-big" value="ВІДПРАВИТИ" />
						</div>
					</form>
				</div>

				<div class="copyright">&copy;2014 Пройдисвіт</div>
			</div>
		</div>
	</footer>

	<div class="zHiddenBlock">
		<div id="order" class="popup">
			<div class="popup-heading mainheading">ЗАЯВКА НА ПОДОРОЖ</div>
			<form action="" class="zNice popup-form">
				<div class="form-row">
					<div class="form-label"><span class="label-title">Ім'я</span><span class="vfix"></span></div>
					<div class="form-input"><input type="text" required name="uname" data-image="images/icon-human.png" /></div>
				</div>
				<div class="form-row">
					<div class="form-label"><span class="label-title">Номер телефону</span><span class="vfix"></span></div>
					<div class="form-input"><input type="text" required phone name="uphone" data-image="images/icon-phone.png" /></div>
				</div>
				<div class="form-row">
					<div class="form-label"><span class="label-title">Email</span><span class="vfix"></span></div>
					<div class="form-input"><input type="email" required  name="uemail" data-image="images/icon-mail.png" /></div>
				</div>
				<div class="form-submit">
					<input type="submit" value="ВІДПРАВИТИ ЗАЯВКУ"/>
				</div>
			</form>
		</div>
	</div>
</body>
</html>